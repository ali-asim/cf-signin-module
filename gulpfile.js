// -- paths
var path          = './assets/';

var skinSassPath  = path+'styles/',
    input         = skinSassPath+'/**/*.scss';

var cssDest       = path+'dist/css/',
    jsFiles       = path+'js/**/*',
    jsDest        = path+'dist/js/';

// define
var gulp          = require('gulp');
var sass          = require('gulp-sass');
var autoprefixer  = require('gulp-autoprefixer');
var cleancss      = require('gulp-clean-css');
var sourcemaps    = require('gulp-sourcemaps');
var concat        = require('gulp-concat');
var minify        = require('gulp-minify');
var browserSync   = require('browser-sync').create();
var reload        = browserSync.reload;

// tasks
// -- sass
gulp.task('sass', function () {
  return gulp.src(input)
  .pipe(sourcemaps.init())
	.pipe(sass().on('error', sass.logError))
  .pipe(autoprefixer())
  .pipe(sourcemaps.write('maps/'))
  .pipe(cleancss())
	.pipe(gulp.dest(cssDest))
	.pipe(browserSync.stream()); 
});
// -- scripts
gulp.task('scripts',function(){
  gulp.src([path+'/js/vendor/firebase.js',
            path+'/js/vendor/jquery.min.js',
            path+'/js/vendor/parsley.min.js',
            path+'/js/main.babel.js'
  ])
	.pipe(concat('scripts.js'))
	.pipe(minify())
	.pipe(gulp.dest(jsDest));
});

// -- watch
gulp.task('watch', function() {
	browserSync.init({
    server: './',
	  files: input
  });
  // watch
  gulp.watch(input, ['sass'])
	.on('change', function(event){
		console.log('File' + event.path + ' was ' + event.type + ', running tasks...')
	});
});
gulp.task('default', ['sass', 'watch']);