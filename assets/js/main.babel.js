'use strict';
(function(){
  // Initialize Firebase
  let config = {
    apiKey: "AIzaSyDxb9Cqu9RhlHrFewMdbBCwRbLDh0viv3E",
    authDomain: "cfuaetest.firebaseapp.com",
    databaseURL: "https://cfuaetest.firebaseio.com",
    projectId: "cfuaetest",
    storageBucket: "cfuaetest.appspot.com",
    messagingSenderId: "160359850967"
  };
  // module element
  const $el = jQuery('.__signin-form');
  // module object
  const module_signin_form = {
    init: function() {
      this.cacheDom();
      this.bindEvents();
    },
    cacheDom: function() {
        this.$form = $el.find('form');
        this.$formInputWrap = $el.find('.input');
        this.$formInput = $el.find('.input input');
        this.$button = $el.find('button');
        $el.addClass('active');
    },
    bindEvents: function() {
      this.$form.on('submit', this.formValidate.bind(this));
      this.$formInput.keypress(function(){  
        jQuery(this).parent().addClass('input-active');
        if(jQuery(this).hasClass('parsley-error')){
          jQuery(this).parent().addClass('parsley-error-wrap');
        } else{
          jQuery(this).parent().removeClass('parsley-error-wrap');
        }
      });
      this.$formInput.on('change', function(){  
        if( !this.value ) {
          jQuery(this).parent().removeClass('input-active');
        }
        if(jQuery(this).hasClass('parsley-error')){
          jQuery(this).parent().addClass('parsley-error-wrap');
        } else{
          jQuery(this).parent().removeClass('parsley-error-wrap');
        }
      });
    },
    formValidate: function(e) {
        e.preventDefault();
        this.$form.parsley().validate();
        if (this.$form.parsley().isValid()){
          this.ajaxRequest();
        } else {
          return false; 
        }
    },
    ajaxRequest : function() {
      let $formData = this.$form.serialize();
      $.ajax({
        url: 'https://cfuaetest.firebaseio.com/.json',
        type: "POST",
        data      : JSON.stringify($formData),
        dataType 	: 'json', 
        encode 		: true,
        success: function () {
          $el.addClass('success');
          setTimeout(() => {
            $el.removeClass('active');
          }, 300);
        },
        error: function(error) {
          alert("error: "+error);
        }
      })
    }
  };
  // initialize
  firebase.initializeApp(config);
  module_signin_form.init();
})();