# README #

### How to setup on local environment? ###

* Clone Repo
* Browse into the directory via terminal - type - npm install
* then install - gulp watch
* this will launch the app on your favorite browser 

* You can always just run the app directly from chrome by opening "index.html"

### What to look for? ###
* js is under assets/js/main-babel.js
* scss is under assets/styles/

### External libraries I have used and more ###
* jQuery, Parsley and Firebase(was not part of the scope)
* I have added Firebase api just for fun really :)
* I have kept things as modular as possible both in js and scss 

### Tools used ###
Visual Studio Code as the editor
Gulp as a task runner
Photoshop for design

I hope to hear from you soon :) thank you for the consideration.
